set -e
export TAR_OPTIONS="--owner root --group root --mode a+rX"
export GZIP="-9n"
pwd=$(pwd)
version="$1"
if [ -z "$version" ]
then
    printf 'Usage: %s <version>\n' "$0"
    exit 1
fi
cd "$(dirname "$0")/../"
tmpdir=$(mktemp -d get-orig-source.XXXXXX)
uscan --noconf --force-download --rename --download-version="$version" --destdir="$tmpdir"
cd "$tmpdir"
tar -xzf pdfminer_*.orig.tar.gz
rm *.tar.gz
# Remove test documents without source:
rm -Rf pdfminer-*/samples/nonfree/
# Remove byte-compiled files
find pdfminer-* -name '*.py[co]' -delete
mv pdfminer-*/ "pdfminer-$version.orig"
tar -czf "$pwd/pdfminer_$version+dfsg.orig.tar.gz" pdfminer-*.orig/
cd ..
rm -Rf "$tmpdir"

# vim:ts=4 sw=4 et
